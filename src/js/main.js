$(document).ready(function () {

  $('.site_loader').fadeOut()
  $('body').addClass('fadeIn')
  var myFullpage = new fullpage('#fullpage', {
    //Navigation
    scrollHorizontally: !1,
    navigation: true,
    controlArrows: !1,
    loopHorizontal: !1,
    slidesNavigation: !0,
    responsiveWidth: 1200,
    responsiveSlides: !0,
    keyboardScrolling: !1,
    responsive: 1200,
    lazyLoading: !1,
    //events
    onLeave: function (origin, destination, direction) {
      if (destination.index == 1 || destination.index == 3) {
        $('.shapes img').removeClass('fadeInLeft')
        $('.traingler_holder').removeClass('fadeInUp rotateIn').addClass('fadeOutDown')
        $('.navbar').fadeOut()
        $('.fp-right').addClass('colored')
      } else if (destination.index == 2 || destination.index == 0) {
        $('.shapes img').addClass('fadeInLeft')
        $('.traingler_holder').addClass('fadeInUp rotateIn')
        $('.navbar').fadeIn()
        $('.fp-right').removeClass('colored')
      }
      typeWriting()
    },
    afterLoad: function (origin, destination, direction) {
      typeWriting()
    },
    afterRender: function () {},
    afterResize: function (width, height) {},
    afterReBuild: function () {},
    afterResponsive: function (isResponsive) {},
    afterSlideLoad: function (section, origin, destination, direction) {},
    onSlideLeave: function (section, origin, destination, direction) {}
  });


  function typeWriting() {
    $('.typewriter').each(function () {
      var typewriter = new Typewriter(this, {
        strings: $(this).data('strings'),
        autoStart: true,
        loop: true
      });
    })
  }
});